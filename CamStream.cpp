#include "stdafx.h"

int _tmain2(int argc, _TCHAR* argv[]);

volatile bool running;

#define M_PI       3.14159265358979323846

int my_write_packet(void *opaque, uint8_t *buf, int buf_size)
{
    int n = 0;
    printf("write %d bytes\n", buf_size);
    return buf_size;
}

static void __cdecl dtor(void)
{
    printf("destructor\n");
    //system("pause");
    if (running)
        exit(0);
}

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "libavutil/channel_layout.h"
#include "libavutil/mathematics.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"

/* 5 seconds stream duration */
#define STREAM_DURATION   15.0
#define STREAM_FRAME_RATE 25 /* 25 images/s */
#define STREAM_NB_FRAMES  ((int)(STREAM_DURATION * STREAM_FRAME_RATE))
#define STREAM_PIX_FMT    AV_PIX_FMT_YUV420P /* default pix_fmt */
#define SCALE_FLAGS SWS_BICUBIC

// a wrapper around a single output AVStream
typedef struct OutputStream {
    AVStream *st;
    /* pts of the next frame that will be generated */
    int64_t next_pts;
    AVFrame *frame;
    AVFrame *tmp_frame;
    float t, tincr, tincr2;
    struct SwsContext *sws_ctx;
} OutputStream;
/**************************************************************/
/* audio output */
/*
* add an audio output stream
*/
static void add_audio_stream(OutputStream *ost, AVFormatContext *oc,
enum AVCodecID codec_id)
{
    AVCodecContext *c;
    AVCodec *codec;
    /* find the audio encoder */
    codec = avcodec_find_encoder(codec_id);
    if (!codec) {
        fprintf(stderr, "codec not found\n");
        exit(1);
    }
    ost->st = avformat_new_stream(oc, codec);
    if (!ost->st) {
        fprintf(stderr, "Could not alloc stream\n");
        exit(1);
    }
    c = ost->st->codec;
    /* put sample parameters */
    c->sample_fmt = AV_SAMPLE_FMT_S16;
    c->bit_rate = 64000;
    c->sample_rate = 44100;
    c->channels = 2;
    c->channel_layout = AV_CH_LAYOUT_STEREO;
    // some formats want stream headers to be separate
    if (oc->oformat->flags & AVFMT_GLOBALHEADER)
        c->flags |= CODEC_FLAG_GLOBAL_HEADER;
}
static void open_audio(AVFormatContext *oc, OutputStream *ost)
{
    AVCodecContext *c;
    int ret;
    c = ost->st->codec;
    /* open it */
    if (avcodec_open2(c, NULL, NULL) < 0) {
        fprintf(stderr, "could not open codec\n");
        exit(1);
    }
    /* init signal generator */
    ost->t = 0;
    ost->tincr = 2 * M_PI * 110.0 / c->sample_rate;
    /* increment frequency by 110 Hz per second */
    ost->tincr2 = 2 * M_PI * 110.0 / c->sample_rate / c->sample_rate;
    ost->frame = av_frame_alloc();
    if (!ost->frame)
        exit(1);
    ost->frame->sample_rate = c->sample_rate;
    ost->frame->format = AV_SAMPLE_FMT_S16;
    ost->frame->channel_layout = c->channel_layout;
    if (c->codec->capabilities & CODEC_CAP_VARIABLE_FRAME_SIZE)
        ost->frame->nb_samples = 10000;
    else
        ost->frame->nb_samples = c->frame_size;
    ret = av_frame_get_buffer(ost->frame, 0);
    if (ret < 0) {
        fprintf(stderr, "Could not allocate an audio frame.\n");
        exit(1);
    }
}
/* Prepare a 16 bit dummy audio frame of 'frame_size' samples and
* 'nb_channels' channels. */
static AVFrame *get_audio_frame(OutputStream *ost)
{
    int j, i, v, ret;
    int16_t *q = (int16_t*)ost->frame->data[0];
    /* check if we want to generate more frames */
    if (av_compare_ts(ost->next_pts, ost->st->codec->time_base,
        STREAM_DURATION, { 1, 1 }) >= 0)
        return NULL;
    /* when we pass a frame to the encoder, it may keep a reference to it
    * internally;
    * make sure we do not overwrite it here
    */
    ret = av_frame_make_writable(ost->frame);
    if (ret < 0)
        exit(1);
    for (j = 0; j < ost->frame->nb_samples; j++) {
        v = (int)(sin(ost->t) * 10000);
        for (i = 0; i < ost->st->codec->channels; i++)
            *q++ = v;
        ost->t += ost->tincr;
        ost->tincr += ost->tincr2;
    }
    ost->frame->pts = ost->next_pts;
    ost->next_pts += ost->frame->nb_samples;
    return ost->frame;
}
/*
* encode one audio frame and send it to the muxer
* return 1 when encoding is finished, 0 otherwise
*/
static int write_audio_frame(AVFormatContext *oc, OutputStream *ost)
{
    AVCodecContext *c;
    AVPacket pkt = { 0 }; // data and size must be 0;
    AVFrame *frame;
    int got_packet;
    av_init_packet(&pkt);
    c = ost->st->codec;
    frame = get_audio_frame(ost);
    avcodec_encode_audio2(c, &pkt, frame, &got_packet);
    if (got_packet) {
        pkt.stream_index = ost->st->index;
        /* Write the compressed frame to the media file. */
        if (av_interleaved_write_frame(oc, &pkt) != 0) {
            fprintf(stderr, "Error while writing audio frame\n");
            exit(1);
        }
    }
    return (frame || got_packet) ? 0 : 1;
}
/**************************************************************/
/* video output */
/* Add a video output stream. */
static void add_video_stream(OutputStream *ost, AVFormatContext *oc,
enum AVCodecID codec_id)
{
    AVCodecContext *c;
    AVCodec *codec;
    /* find the video encoder */
    codec = avcodec_find_encoder(codec_id);
    if (!codec) {
        fprintf(stderr, "codec not found\n");
        exit(1);
    }
    ost->st = avformat_new_stream(oc, codec);
    if (!ost->st) {
        fprintf(stderr, "Could not alloc stream\n");
        exit(1);
    }
    c = ost->st->codec;
    /* Put sample parameters. */
    c->bit_rate = 400000;
    /* Resolution must be a multiple of two. */
    c->width = 352;
    c->height = 288;
    /* timebase: This is the fundamental unit of time (in seconds) in terms
    * of which frame timestamps are represented. For fixed-fps content,
    * timebase should be 1/framerate and timestamp increments should be
    * identical to 1. */
    c->time_base.den = STREAM_FRAME_RATE;
    c->time_base.num = 1;
    c->gop_size = 12; /* emit one intra frame every twelve frames at most */
    c->pix_fmt = STREAM_PIX_FMT;
    if (c->codec_id == AV_CODEC_ID_MPEG2VIDEO) {
        /* just for testing, we also add B frames */
        c->max_b_frames = 2;
    }
    if (c->codec_id == AV_CODEC_ID_MPEG1VIDEO) {
        /* Needed to avoid using macroblocks in which some coeffs overflow.
        * This does not happen with normal video, it just happens here as
        * the motion of the chroma plane does not match the luma plane. */
        c->mb_decision = 2;
    }
    /* Some formats want stream headers to be separate. */
    if (oc->oformat->flags & AVFMT_GLOBALHEADER)
        c->flags |= CODEC_FLAG_GLOBAL_HEADER;
}
static AVFrame *alloc_picture(enum AVPixelFormat pix_fmt, int width, int height)
{
    AVFrame *picture;
    int ret;
    picture = av_frame_alloc();
    if (!picture)
        return NULL;
    picture->format = pix_fmt;
    picture->width = width;
    picture->height = height;
    /* allocate the buffers for the frame data */
    ret = av_frame_get_buffer(picture, 32);
    if (ret < 0) {
        fprintf(stderr, "Could not allocate frame data.\n");
        exit(1);
    }
    return picture;
}
static void open_video(AVFormatContext *oc, OutputStream *ost)
{
    AVCodecContext *c;
    c = ost->st->codec;
    /* open the codec */
    if (avcodec_open2(c, NULL, NULL) < 0) {
        fprintf(stderr, "could not open codec\n");
        exit(1);
    }
    /* Allocate the encoded raw picture. */
    ost->frame = alloc_picture(c->pix_fmt, c->width, c->height);
    if (!ost->frame) {
        fprintf(stderr, "Could not allocate picture\n");
        exit(1);
    }
    /* If the output format is not YUV420P, then a temporary YUV420P
    * picture is needed too. It is then converted to the required
    * output format. */
    ost->tmp_frame = NULL;
    if (c->pix_fmt != AV_PIX_FMT_YUV420P) {
        ost->tmp_frame = alloc_picture(AV_PIX_FMT_YUV420P, c->width, c->height);
        if (!ost->tmp_frame) {
            fprintf(stderr, "Could not allocate temporary picture\n");
            exit(1);
        }
    }
}
/* Prepare a dummy image. */
static void fill_yuv_image(AVFrame *pict, int frame_index,
    int width, int height)
{
    int x, y, i, ret;
    /* when we pass a frame to the encoder, it may keep a reference to it
    * internally;
    * make sure we do not overwrite it here
    */
    ret = av_frame_make_writable(pict);
    if (ret < 0)
        exit(1);
    i = frame_index;
    /* Y */
    for (y = 0; y < height; y++)
    for (x = 0; x < width; x++)
        pict->data[0][y * pict->linesize[0] + x] = x + y + i * 3;
    /* Cb and Cr */
    for (y = 0; y < height / 2; y++) {
        for (x = 0; x < width / 2; x++) {
            pict->data[1][y * pict->linesize[1] + x] = 128 + y + i * 2;
            pict->data[2][y * pict->linesize[2] + x] = 64 + x + i * 5;
        }
    }
}
static AVFrame *get_video_frame(OutputStream *ost)
{
    AVCodecContext *c = ost->st->codec;
    /* check if we want to generate more frames */
    if (av_compare_ts(ost->next_pts, ost->st->codec->time_base,
        STREAM_DURATION, { 1, 1 }) >= 0)
        return NULL;
    if (c->pix_fmt != AV_PIX_FMT_YUV420P) {
        /* as we only generate a YUV420P picture, we must convert it
        * to the codec pixel format if needed */
        if (!ost->sws_ctx) {
            ost->sws_ctx = sws_getContext(c->width, c->height,
                AV_PIX_FMT_YUV420P,
                c->width, c->height,
                c->pix_fmt,
                SCALE_FLAGS, NULL, NULL, NULL);
            if (!ost->sws_ctx) {
                fprintf(stderr,
                    "Cannot initialize the conversion context\n");
                exit(1);
            }
        }
        fill_yuv_image(ost->tmp_frame, ost->next_pts, c->width, c->height);
        sws_scale(ost->sws_ctx, ost->tmp_frame->data, ost->tmp_frame->linesize,
            0, c->height, ost->frame->data, ost->frame->linesize);
    }
    else {
        fill_yuv_image(ost->frame, ost->next_pts, c->width, c->height);
    }
    ost->frame->pts = ost->next_pts++;
    return ost->frame;
}
/*
* encode one video frame and send it to the muxer
* return 1 when encoding is finished, 0 otherwise
*/
static int write_video_frame(AVFormatContext *oc, OutputStream *ost)
{
    int ret;
    AVCodecContext *c;
    AVFrame *frame;
    int got_packet = 0;
    c = ost->st->codec;
    frame = get_video_frame(ost);
    if (oc->oformat->flags & AVFMT_RAWPICTURE) {
        /* a hack to avoid data copy with some raw video muxers */
        AVPacket pkt;
        av_init_packet(&pkt);
        if (!frame)
            return 1;
        pkt.flags |= AV_PKT_FLAG_KEY;
        pkt.stream_index = ost->st->index;
        pkt.data = (uint8_t *)frame;
        pkt.size = sizeof(AVPicture);
        pkt.pts = pkt.dts = frame->pts;
        //av_packet_rescale_ts(&pkt, c->time_base, ost->st->time_base);
        ret = av_interleaved_write_frame(oc, &pkt);
    }
    else {
        AVPacket pkt = { 0 };
        av_init_packet(&pkt);
        /* encode the image */
        ret = avcodec_encode_video2(c, &pkt, frame, &got_packet);
        if (ret < 0) {
            fprintf(stderr, "Error encoding a video frame\n");
            exit(1);
        }
        if (got_packet) {
            //av_packet_rescale_ts(&pkt, c->time_base, ost->st->time_base);
            pkt.stream_index = ost->st->index;
            /* Write the compressed frame to the media file. */
            ret = av_interleaved_write_frame(oc, &pkt);
        }
    }
    if (ret != 0) {
        fprintf(stderr, "Error while writing video frame\n");
        exit(1);
    }
    return (frame || got_packet) ? 0 : 1;
}
static void close_stream(AVFormatContext *oc, OutputStream *ost)
{
    avcodec_close(ost->st->codec);
    av_frame_free(&ost->frame);
    av_frame_free(&ost->tmp_frame);
    sws_freeContext(ost->sws_ctx);
}
/**************************************************************/
/* media file output */
int main(int argc, char **argv)
{
    atexit(dtor);
    return _tmain2(argc, argv);

    OutputStream video_st = { 0 }, audio_st = { 0 };
    const char *filename;
    AVOutputFormat *fmt;
    AVFormatContext *oc;
    int have_video = 0, have_audio = 0;
    int encode_video = 0, encode_audio = 0;
    int i;
    /* Initialize libavcodec, and register all codecs and formats. */
    av_register_all();
    if (argc != 2) {
        printf("usage: %s output_file\n"
            "API example program to output a media file with libavformat.\n"
            "The output format is automatically guessed according to the file extension.\n"
            "Raw images can also be output by using '%%d' in the filename\n"
            "\n", argv[0]);
        return 1;
    }
    filename = argv[1];
    /* Autodetect the output format from the name. default is MPEG. */
    fmt = av_guess_format(NULL, filename, NULL);
    if (!fmt) {
        printf("Could not deduce output format from file extension: using MPEG.\n");
        fmt = av_guess_format("mpeg", NULL, NULL);
    }
    if (!fmt) {
        fprintf(stderr, "Could not find suitable output format\n");
        return 1;
    }
    /* Allocate the output media context. */
    oc = avformat_alloc_context();
    if (!oc) {
        fprintf(stderr, "Memory error\n");
        return 1;
    }
    oc->oformat = fmt;
    sprintf_s(oc->filename, sizeof(oc->filename), "%s", filename);
    /* Add the audio and video streams using the default format codecs
    * and initialize the codecs. */
    if (fmt->video_codec != AV_CODEC_ID_NONE) {
        add_video_stream(&video_st, oc, fmt->video_codec);
        have_video = 1;
        encode_video = 1;
    }
    //if (fmt->audio_codec != AV_CODEC_ID_NONE) {
    //    add_audio_stream(&audio_st, oc, fmt->audio_codec);
    //    have_audio = 1;
    //    encode_audio = 1;
    //}
    /* Now that all the parameters are set, we can open the audio and
    * video codecs and allocate the necessary encode buffers. */
    if (have_video)
        open_video(oc, &video_st);
    if (have_audio)
        open_audio(oc, &audio_st);
    //av_dump_format(oc, 0, filename, 1);
    /* open the output file, if needed */
    if (!(fmt->flags & AVFMT_NOFILE)) {
        if (avio_open(&oc->pb, filename, AVIO_FLAG_WRITE) < 0) {
            fprintf(stderr, "Could not open '%s'\n", filename);
            return 1;
        }
    }
    /* Write the stream header, if any. */
    int ret = avformat_write_header(oc, NULL);
    while (0&&encode_video || encode_audio) {
        /* select the stream to encode */
        if (encode_video &&
            (!encode_audio || av_compare_ts(video_st.next_pts, video_st.st->codec->time_base,
            audio_st.next_pts, audio_st.st->codec->time_base) <= 0)) {
            encode_video = !write_video_frame(oc, &video_st);
        }
        else {
            encode_audio = !write_audio_frame(oc, &audio_st);
        }
    }
    /* Write the trailer, if any. The trailer must be written before you
    * close the CodecContexts open when you wrote the header; otherwise
    * av_write_trailer() may try to use memory that was freed on
    * av_codec_close(). */
    av_write_trailer(oc);
    /* Close each codec. */
    if (have_video)
        close_stream(oc, &video_st);
    if (have_audio)
        close_stream(oc, &audio_st);
    if (!(fmt->flags & AVFMT_NOFILE))
        /* Close the output file. */
        avio_close(oc->pb);
    /* free the stream */
    avformat_free_context(oc);
    return 0;
}

/*
Useful links:
https://libav.org/doxygen/master/output_8c-example.html#a112
http://www.ffmpeg.org/doxygen/2.0/doc_2examples_2muxing_8c-example.html#a135 (example above)
http://stackoverflow.com/questions/9604633/reading-a-file-located-in-memory-with-libavformat (avio)
*/

int _tmain2(int argc, _TCHAR* argv[])
{
    int ret;
    const char* filename = "out.mp4";

    atexit(dtor);
    avformat_network_init();
    avcodec_register_all();
    av_register_all();

    AVFormatContext* format;
    AVCodecContext* ctx;
    AVCodec* encoder;
    AVFrame* frame;
    AVStream* stream;

    cv::VideoCapture cap(0);
    int width = (int)cap.get(CV_CAP_PROP_FRAME_WIDTH);
    int height = (int)cap.get(CV_CAP_PROP_FRAME_HEIGHT);

    format = avformat_alloc_context();

    //strcpy_s(format->filename, 1024, filename);
    format->oformat = av_guess_format("rts", nullptr, nullptr);

    encoder = avcodec_find_encoder(format->oformat->video_codec);
    stream = avformat_new_stream(format, encoder);

    ctx = stream->codec;
    stream->time_base = { 1, 25 };
    ctx->bit_rate = 400000;
    ctx->width = width;
    ctx->height = height;
    ctx->gop_size = 12;
    ctx->time_base = { 1, 25 };
    //ctx->max_b_frames = 2;
    //ctx->mb_decision = 2;
    ctx->pix_fmt = AV_PIX_FMT_YUV420P;

    if (format->oformat->flags & AVFMT_GLOBALHEADER)
        ctx->flags |= CODEC_FLAG_GLOBAL_HEADER;

    ret = avcodec_open2(ctx, nullptr, nullptr);

    //*open the output file, if needed * /
    //if (!(format->oformat->flags & AVFMT_NOFILE))
    {
        ret = avio_open(&format->pb, filename, AVIO_FLAG_WRITE);
        //uint8_t *buffer = (uint8_t*)av_malloc(32768);
        //format->pb = avio_alloc_context(buffer, 32768, 1, nullptr, nullptr, my_write_packet, nullptr);
        //format->pb->write_packet = my_write_packet;
    }

    ret = avformat_write_header(format, nullptr);
    
    //frame = av_frame_alloc();
    //frame->format = ctx->pix_fmt;
    //frame->width = width;
    //frame->height = height;
    //ret = av_frame_get_buffer(frame, 32);
    frame = alloc_picture(ctx->pix_fmt, width, height);

    //int frameSize = avpicture_get_size(AV_PIX_FMT_BGR24, width, height);
    //uint8_t* frameData = static_cast<uint8_t*>(av_malloc(frameSize));

    running = true;
    
    int i = 0;

    //packet.size = 100000;
    //packet.data = malloc(packet.size);

    cv::namedWindow("preview");
    while (running && cv::waitKey(10) != 27)
    {
        cv::Mat capFrame;
        if (cap.read(capFrame))
        {
            cv::cvtColor(capFrame, capFrame, CV_BGR2YUV_I420);
            cv::imshow("preview", capFrame);

            avpicture_fill((AVPicture*)frame, capFrame.data, AV_PIX_FMT_YUV420P, ctx->width, ctx->height);

            AVPacket packet = { 0 };
            av_init_packet(&packet);
            //fill_yuv_image(frame, i, width, height);
            frame->pts = i;
            //* encode the image * /
            int packetReady = 0;
            ret = avcodec_encode_video2(ctx, &packet, frame, &packetReady);
            if (packetReady)
            {
                packet.stream_index = stream->index;
                ret = av_write_frame(format, &packet);
                printf("packet\n");
                av_write_trailer(format);
            }

            i++;
        }
    }

    av_write_trailer(format);
    avio_close(format->pb);

    //* get the delayed frames * /
    //for (; out_size; i++) 
    //{
    //    fflush(stdout);

    //    out_size = avcodec_encode_video2(ctx, outbuf, outbuf_size, NULL);
    //    printf("write frame %3d (size=%5d)\n", i, out_size);
    //    fwrite(outbuf, 1, out_size, f);
    //}

    //* add sequence end code to have a real mpeg file * /
    //outbuf[0] = 0x00;
    //outbuf[1] = 0x00;
    //outbuf[2] = 0x01;
    //outbuf[3] = 0xb7;
    //fwrite(outbuf, 1, 4, f);
    //fclose(f);
    //free(picture_buf);
    //free(outbuf);

    cap.release();
    running = false;
    return 0;
    
}

